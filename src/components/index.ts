import SvgIcon from "./SvgIcon/index.vue"
import Test from "./test.vue"
import Category from "./Category/inde.vue"

// 引入element-plus 所有图标
// 注册所有图标有两种方法，一是 跟自定义的组件一起注册，二是 单独循环 所有图标进行注册
import * as ElementPlusIconsVue from '@element-plus/icons-vue'

let allGloabComponent ={
  SvgIcon,
  Test,
  Category,
  ...ElementPlusIconsVue  // 法一：跟自定义组件一起注册图标
}
//对外暴露插件对象
export default {
  //install 方法会在页面初始化时候执行
  install(app) {
    // console.log("allGloabComponent")
    Object.keys(allGloabComponent).forEach(key => {
      app.component(key, allGloabComponent[key])
    })
    // 法二、注册element-plus所有图标
    // for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    //   app.component(key, component)
    // }
  }
}