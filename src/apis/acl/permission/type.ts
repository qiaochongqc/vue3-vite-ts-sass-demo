export interface ResponseData {
  code: number,
  message: string,
  ok: boolean
}

export interface PermissionData {
  id?: number,
  createTime?: string,
  updateTime?: string,
  pid?: number,
  name: string,
  code: string | null,
  toCode?: null,
  type?: number,
  status?: null,
  level?: number,
  children?: PermissionChildren,
  select?: boolean
}

export type PermissionChildren = PermissionData[]

export interface ResponsePermission extends ResponseData {
  data: PermissionData[]
}