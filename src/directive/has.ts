
import useUserStore from "@/store/modules/user"
export const isHasButton = (app:any) => {
  // 自定义指令，判断按钮是否有操作权限
  app.directive('has', {
    // 使用这个指令的dom在挂载时会执行一次
    mounted(el:any,options:any) {
      // 在所有权限按钮中匹配不到指定绑定的权限时，将元素删除
      if(!useUserStore().buttons.includes(options.value)) {
        // el.parentElement.removeChild(el)
        el.remove()
      }
    },
  })
}