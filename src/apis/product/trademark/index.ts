// 引入reqire
import request from "@/utils/request"

// 引入品牌类型
import type { ResponsBaseTrademark, Trademark }  from './type'

// 枚举 接口地址
enum API {
  requestBaseTrademark = "/admin/product/baseTrademark",
  saveBaseTrademark = "/admin/product/baseTrademark/save",
  updateBaseTrademark = "/admin/product/baseTrademark/update",
  deleteBaseTrademark = "/admin/product/baseTrademark/remove"
}

export const reqBaseTrademark = (page:number, limit:number) => request.get<any,ResponsBaseTrademark>(`${API.requestBaseTrademark}/${page}/${limit}`)
export const saveOrUpdateBaseTrademark = (data: Trademark) => {
  if(data.id) {
    return request.put(API.updateBaseTrademark,data)
  } else {
    return request.post(API.saveBaseTrademark,data)
  }
}

export const deleteBaseTrademark = (id:number) => request.delete(`${API.deleteBaseTrademark}/${id}`)