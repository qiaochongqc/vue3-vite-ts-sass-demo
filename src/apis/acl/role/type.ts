export interface ResponseData {
  code: number,
  message: string,
  ok: string
}

export interface Role {
  id?: number,
  createTime?: string,
  updateTime?: string,
  roleName: string,
}

export type RoleList = Role[]

export interface ResponseRoleList extends ResponseData {
  data: {
    records: RoleList,
    total: number,
    size: number,
    current: number,
    pages: number
  }
}

export interface PermissionData {
  id:1,
  createTime: string,
  updateTime:string,
  pid:number,
  name:string,
  code:null,
  toCode:null,
  type:number,
  status:null,
  level:number,
  children?: PermissionList,
  select: boolean
}

export type PermissionList = PermissionData[]

export interface ResponsePermissionData extends ResponseData {
  data: PermissionData[]
}