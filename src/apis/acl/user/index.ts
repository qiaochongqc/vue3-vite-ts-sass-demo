import request from "@/utils/request";
import type { ResponseData, User, RespondeUserRole, DoAssignParams } from "./type"
enum API {
  // 用户列表
  USERLIST_URL = "/admin/acl/user",
  // 新增用户
  ADDUSER_URL = "/admin/acl/user/save",
  // 更新用户信息
  UPDATEUSER_URL = "/admin/acl/user/update",
  // 获取用户角色、全部角色接口
  USERROLE_URL = "/admin/acl/user/toAssign",
  // 给用户分配角色
  DOASSIGNROLE_URL = "/admin/acl/user/doAssignRole",
  // 删除用户
  REMOVEUSER_URL = "/admin/acl/user/remove",
  // 批量删除用户
  BATCHREMOVE_URL = "/admin/acl/user/batchRemove"
}

export const reqUserList = (page:number, limit:number, username:string) => request.get<any, ResponseData>(`${API.USERLIST_URL}/${page}/${limit}?username=${username}`)

export const reqAddOrUpdateUser = (data: User) => {
  if(data.id) {
    return request.put(API.UPDATEUSER_URL, data)
  } else {
    return request.post(API.ADDUSER_URL, data)
  }
}

export const reqUserRole = (userId: string|number) => request.get<any, RespondeUserRole>(`${API.USERROLE_URL}/${userId}`)

export const reqDoAssignRole = (data:DoAssignParams) => request.post<any,any>(API.DOASSIGNROLE_URL, data)

export const reqDeleteUser = (userId:number) => request.delete(`${API.REMOVEUSER_URL}/${userId}`)

export const reqBatchDeleteUser = (idList:any) => request.delete<any,any>(API.BATCHREMOVE_URL, {data: idList})