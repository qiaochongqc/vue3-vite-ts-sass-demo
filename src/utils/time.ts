export const getTime = () => {
  let message = "",
    hours = new Date().getHours();
  if(hours < 9) {
    message = "上午"
  } else if (hours < 12) {
    message = "中午"
  } else if (hours < 18) {
    message = "下午"
  } else if (hours < 24) {
    message = "晚上"
  } else {
    message = "早上"
  }
  return message
}