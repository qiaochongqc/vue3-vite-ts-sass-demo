export interface ResponseData {
  code: number,
  message: string,
  ok: boolean
}

export interface CategoryObj {
  category1Id?: number,
  category2Id?: number,
  id: number | string,
  createTime: string,
  updateTime: string,
  name: string
}

export interface CategoryResponseData extends ResponseData {
  data: CategoryObj[]
}

// 属性值
export interface AttrValue {
  id?: number | null,
  createTime?: string | null,
  updateTime?: string | null,
  valueName?: string,
  attrId?: number | null,
  edit?: boolean
}
// 属性值列表
export type AttrValueList = AttrValue[]
// 属性对象
export interface AttrObj {
  id?: number,
  createTime?: null | string,
  updateTime?: null | string,
  attrName: string,
  categoryId: number | string,
  categoryLevel: number | string,
  attrValueList: AttrValueList
}
export type AttrList = AttrObj[]
// 接口返回的属性数据
export interface ResponseAttr extends ResponseData {
  data: AttrObj[]
}