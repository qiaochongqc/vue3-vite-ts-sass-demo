// 接口响应的基本类型
export interface ResponseData {
  code: number,
  message: string,
  ok: true
}
// 已有品牌的类型
export interface Trademark {
  id?: number,
  createTime?: string,
  updateTime?: string,
  tmName: string,
  logoUrl: string
}
// 已有品牌列表的类型
export type Records = Trademark[]

export interface ResponsBaseTrademark extends ResponseData{
  data: {
    records: Records
    total: number,
    size: number,
    current: number,
    orders: [],
    optimizeCountSql: boolean,
    hitCount: boolean,
    countId: null,
    maxLimit: null,
    searchCount: boolean,
    pages: number
  }
}