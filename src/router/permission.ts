/**
 * 路由里面需处理以下3个问题
 * 1、任意路由切换实现进度条业务 --nprogress
 * 2、路由鉴权（路由组件访问权限的设置）
 *    全部路由组件：登录｜404｜任意路由｜首页｜数据大屏｜权限管理（三个子路由）｜商品管理（四个子路由）
 *    路由鉴权份两种情况（用户是否登录，可以通过 token 判断）：
 *        1）用户未登录：可以访问登录路由（/login），其余路由不能访问
 *        2）用户登录成功：除了登录路由，其余路由都可访问
 */

// 引入路由
import router from "@/router";
// 引入页面进度条方法及样式
import NProgress from "nprogress"
NProgress.configure({ showSpinner: false });
import 'nprogress/nprogress.css'

// 获取用户的store信息，里面包含 token、username
import useUserStore from "@/store/modules/user";

// 配置路由全局前置、后置守卫
router.beforeEach(async (to, from, next) => {
  let userStore = useUserStore()
  // to：要跳转到的页面
  // from：从哪个页面来
  // next：是否放行
  NProgress.start();
  let token = userStore.token;
  let username = userStore.username;
  if(token) {
    //如果存在 token，表示用户已登录

    //登录状态下，如果访问 login 页面，重定向到 home 页面
    if(to.path === "/login") {
      next({ path: "/home" });
    } else {
      // 登录状态下访问其他页面时
      // 如果存在 username，那么路由正常跳转
      if(username) {
        next();
      } else {
        // 如果没有 usernam，那么先获取用户信息，成功后路由再继续跳转
        try {
          await userStore.userInfo()
          // console.log(to)
          // userStore.userInfo() 方法里面会进行异步路由处理，异步路由页面刷新时，
          // 有可能获取到用户信息，异步路由还没加载完毕，可能会出现空白效果
          // next 方法里面加 {...to} 是保证异步路由加载完成以后进行页面路由跳转
          next({...to});
        } catch(error) {
          // 如果获取用户信息失败，那么退出登录，清除用户信息，跳转到登录页面
          // 获取用户信息失败的原因：1、token 失效，2、用户手动修改本地 token
          await userStore.logout();
          next({ path: "/login" });
        }
      }
    }
    
  } else {
    //如果没有token表示用户未登录，未登录状态只能访问 login 页面
    if(to.path === "/login") {
      next();
    } else {
      // 如果访问其他页面，因为没有token重定向了登录页面，标记 redirect 属性，登录成功以后，跳到原页面
      next({ path: "/login", query: { redirect: to.path } })
    }
  }


  
})
router.afterEach((to, from, next) => {
  NProgress.done();
})

