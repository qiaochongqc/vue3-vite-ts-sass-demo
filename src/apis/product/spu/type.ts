export interface ResponseData {
  code: number
  message: string,
  ok: boolean
}

export interface SpuRecords {
  category3Id: number | string,
  createTime?: string,
  description: string,
  id?: number,
  spuName: string,
  tmId: number | string,
  updateTime?: string,
  spuImageList: null | [],
  spuPosterList: null | []
}

export interface SpuListData {
  countId: number | null,
  current: number,
  hitCount: boolean,
  maxLimit: number | null,
  optimizeCountSql: boolean,
  orders: string[],
  pages: number,
  searchCount: boolean,
  size: number,
  total: number,
  records: SpuRecords[]
}

export interface SpuListResponse extends ResponseData{
  data: SpuListData
}
// 商品类型
export interface Trademark {
  id: number,
  logoUrl: string,
  tmName: string
}
// 品牌列表的类型
export type TrademarkList = Trademark[]
// 接口返回的品牌列表的类型
export interface ResponseTrademark extends ResponseData {
  data: TrademarkList
}
// 品牌相关图片的类型
export interface SpuImage {
  id: number,
  spuId: number | string,
  imgName: string,
  imgUrl: string
}
// 品牌相关图片列表的类型
export type SpuImageList = SpuImage[]
// 接口返回的品牌相关图片的类型
export interface ResponseSpuImage extends ResponseData {
  data: SpuImageList
}
// 已有属性值
export interface SpuSaleAttrValue {
  id: number,
  spuId: number,
  baseSaleAttrId: number,
  saleAttrValueName: string,
  saleAttrName: string,
  isChecked: null
}
// 已有属性值列表
export type SpuSaleAttrValueList = SpuSaleAttrValue[]
// 已有属性
export interface SpuSaleAttr {
  id?: number,
  spuId: number,
  baseSaleAttrId: number,
  saleAttrName: string,
  spuSaleAttrValueList: SpuSaleAttrValueList
}
// 接口返回的spu已有属性
export interface ResponseSpuSaleAttr extends ResponseData {
  data: SpuSaleAttr[]
}
export interface BaseSaleAttrValue {
  id: number,
  name: string
}
// 属性下拉列表
export interface ResponseBaseSaleAttrList extends ResponseData {
  data: BaseSaleAttrValue[]
}

// 新增SKU表单数据类型
export interface AddSkuInfo {
  category3Id: number|string,
  spuId: number|string,
  tmId: number|string,
  skuName: string,
  price: number|string,
  weight: number|string,
  skuDesc: string,
  skuAttrValueList: [
    // {
    //   attrId: number|string,
    //   valueId: number|string
    // }
  ],
  skuSaleAttrValueList: [
    // {
    //   saleAttrId: number|string,
    //   saleAttrValueId: number|string
    // }
  ],
  skuDefaultImg: string
}