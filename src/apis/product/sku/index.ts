import request from "@/utils/request";

enum API {
  SKULIST_URL = "/admin/product/list",
  CANCELSALE_URL = "/admin/product/cancelSale",
  ONSALE_URL = "/admin/product/onSale",
  GETSKUINFO_URL = "/admin/product/getSkuInfo",
  DELETESKU_URL = "/admin/product/deleteSku"
}

export const reqSkuList = (page:number|string, limit:number|string) => request.get(`${API.SKULIST_URL}/${page}/${limit}`)
export const reqCacelSale = (skuId:number|string) => request.get(`${API.CANCELSALE_URL}/${skuId}`)
export const reqOnsale = (skuId:number|string) => request.get(`${API.ONSALE_URL}/${skuId}`)
export const reqSkuIndo = (skuId:number|string) => request.get(`${API.GETSKUINFO_URL}/${skuId}`)
export const reqDeleteSku = (skuId:number|string) => request.delete(`${API.DELETESKU_URL}/${skuId}`)