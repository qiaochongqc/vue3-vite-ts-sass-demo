import { RouteRecordRaw } from "vue-router" 

import type { CategoryObj } from "@/apis/product/attr/type"

export interface UserState {
  token: string | null,
  menuRouters: RouteRecordRaw[],
  username: string,
  avatar: string
}

export interface CategoryState {
  c1Arr: CategoryObj[],
  c1Id: string | number,
  c2Arr: CategoryObj[],
  c2Id: string | number,
  c3Arr: CategoryObj[],
  c3Id: string | number,
}