import request from "@/utils/request";

import type { CategoryResponseData, ResponseAttr, AttrObj } from "./type"
import { S } from "mockjs";
// 品牌管理- 属性管理分类的接口
enum API {
  C1_URL = "/admin/product/getCategory1",
  C2_URL = "/admin/product/getCategory2",
  C3_url = "/admin/product/getCategory3",
  ATTR_URL = "/admin/product/attrInfoList",
  ADDORUPDATEATTR = "/admin/product/saveAttrInfo",
  DELETEATTR_URL = "/admin/product/deleteAttr"
}

export const reqC1 = () => request.get<any, CategoryResponseData>(API.C1_URL)
export const reqC2 = (data: number | string) => request.get<any, CategoryResponseData>(`${API.C2_URL}/${data}`)
export const reqC3 = (data: number | string) => request.get<any, CategoryResponseData>(`${API.C3_url}/${data}`)

export const reqAttr = (c1: number | string, c2: number | string, c3: string | number) => request.get<any, ResponseAttr>(`${API.ATTR_URL}/${c1}/${c2}/${c3}`) 
export const reqAddOrUpdateAttrInfo = (data:AttrObj) => request.post<any,any>(API.ADDORUPDATEATTR, data)
export const reqDeleteAttr = (data:number|string) => request.delete(`${API.DELETEATTR_URL}/${data}`)