// 封装本地存储方法
// 封装设置token的方法
export const SET_TOKEN = (token: string) => {
  localStorage.setItem("TOKEN", token)
}
// 封装获取 token 的方法
export const GET_TOKEN = () => {
  return localStorage.getItem("TOKEN")
}
// 删除本地持久化存储 token 的方法
export const REMOVE_TOKEN = () => {
  localStorage.removeItem("TOKEN")
}