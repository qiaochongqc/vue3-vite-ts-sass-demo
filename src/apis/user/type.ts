//定义登录接口入参的类型
export interface loginFormData {
  password: string,
  username: string
}
//定义接口响应的数据类型
interface responseData {
  code: number,
  message: string,
  ok: boolean
}
//定义登录接口响应的数据类型
export interface loginResponseData extends responseData {
  data: string
}
//定义用户信息接口响应的数据类型
export interface userResponseData extends responseData {
  data: {
    routes: string[],
    buttons: string[],
    roles: string[],
    name: string,
    avatar: string
  }
}