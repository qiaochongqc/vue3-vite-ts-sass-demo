//静态路由（所有用户都可以访问的路由：登录、首页、数据大屏、404）
export const constantRoute = [
  {
    path: "/login",
    name: "Login",
    component: () => import("@/views/login/index.vue"),
    meta: {
      title: "登录",  // 菜单标题
      hidden: true, //是否隐藏菜单
      icon: "Notebook"  //菜单图标
    }
  },
  {
    path: "/",
    name: "Layout",
    component: () => import("@/layout/index.vue"),
    redirect: "/home",
    meta: {
      title: "",
      icon: ""
    },
    children: [
      {
        path: "/home",
        name: "Home",
        component: () => import("@/views/home/index.vue"),
        meta: {
          title: "首页",
          icon: "HomeFilled"
        }
      }
    ]
  },
  {
    path: "/404",
    name: "404",
    component: () => import("@/views/404/index.vue"),
    meta: {
      title: "404",
      hidden: true,
      icon: "CirclePlusFilled"
    }
  },
  {
    path: "/screen",
    name: "Screen",
    component: () => import("@/views/screen/index.vue"),
    meta: {
      title: "数据大屏",
      icon: "Platform"
    }
  },
]

// 异步路由（需要根据用户角色过滤的路由：权限管理、商品管理）
export const asyncRoute = [
  {
    path: "/acl",
    name: "Acl",
    component: () => import("@/layout/index.vue"),
    meta: {
      title: "权限管理",
      icon: "Lock"
    },
    redirect: "/acl/user",
    children: [
      {
        path: "/acl/user",
        name: "User",
        component: () => import("@/views/acl/user/index.vue"),
        meta: {
          title: "用户管理",
          icon: "User"
        }
      },
      {
        path: "/acl/role",
        name: "Role",
        component: () => import("@/views/acl/role/index.vue"),
        meta: {
          title: "角色管理",
          icon: "UserFilled"
        }
      },
      {
        path: "/acl/permission",
        name: "Permission",
        component: () => import("@/views/acl/permission/index.vue"),
        meta: {
          title: "菜单管理",
          icon: "Grid"
        }
      }
    ]
  },
  {
    path: "/product",
    name: "Product",
    component: () => import("@/layout/index.vue"),
    meta: {
      title: "商品管理",
      icon: "Goods"
    },
    redirect: "/product/trademark",
    children: [
      {
        path: "/product/trademark",
        name: "Trademark",
        component: () => import("@/views/product/trademark/index.vue"),
        meta: {
          title: "品牌管理",
          icon: "ShoppingTrolley"
        }
      },
      {
        path: "/product/attr",
        name: "Attr",
        component: () => import("@/views/product/attr/index.vue"),
        meta: {
          title: "属性管理",
          icon: "ChatLineSquare"
        }
      },
      {
        path: "/product/spu",
        name: "Spu",
        component: () => import("@/views/product/spu/index.vue"),
        meta: {
          title: "SPU管理",
          icon: "CollectionTag"
        }
      },
      {
        path: "/product/sku",
        name: "Sku",
        component: () => import("@/views/product/sku/index.vue"),
        meta: {
          title: "SKU管理",
          icon: "Shop"
        }
      }
    ]
  }
]

// 任意路由（匹配不上静态路由和异步路由时，跳转到404）
export const anyRoute = [
  {
    // 路由都匹配不上时，匹配这个任意路由，重定向到 404
    path: "/:pathMatch(.*)*",
    redirect: "/404",
    name: "Any",
    meta: {
      title: "其他路由",
      hidden: true,
      icon: "Open"
    }
  }
]