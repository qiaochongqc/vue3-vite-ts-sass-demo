import request from "@/utils/request";

import type { ResponsePermission, PermissionData } from "./type"

enum API {
  //  获取菜单权限
  PERMISSION_URL = "/admin/acl/permission",
  // 添加权限菜单
  ADDPERMISSION_URL = "/admin/acl/permission/save",
  // 更新权限菜单
  UPDATEPERMISSION_URL = "/admin/acl/permission/update",
  // 删除权限菜单
  REMOVEPERSSION_URL = "/admin/acl/permission/remove"
}

export const reqPermission = () => request.get<any, ResponsePermission>(API.PERMISSION_URL)

export const reqAddOrUpdatePermission = (data:PermissionData) => {
  if(data.id) {
    return request.put(API.UPDATEPERMISSION_URL, data)
  } else {
    return request.post(API.ADDPERMISSION_URL, data)
  }
} 

export const reqRemovePermission = (id:number) => request.delete<any, any>(`${API.REMOVEPERSSION_URL}/${id}`)