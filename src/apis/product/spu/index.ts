import request from "@/utils/request";

import type { AddSkuInfo, SpuListResponse, ResponseTrademark, ResponseSpuImage, ResponseSpuSaleAttr, ResponseBaseSaleAttrList } from "./type"
enum API {
  // spu 列表接口
  SPULIST_URL= "/admin/product",
  // 品牌列表接口
  TRADEMARKLIST_URL = "/admin/product/baseTrademark/getTrademarkList",
  // 品牌相关的图片接口
  SPUIMAGELIST_URL= "/admin/product/spuImageList",
  // spu 已有的销售属性表格
  SPUSALEATTRLIST_URL = "/admin/product/spuSaleAttrList",
  // spu 销售属性下拉框
  BASESALEATTRLIST_URL = "/admin/product/baseSaleAttrList",
  // 修改spu
  UPDATESPUINFO_URL = "/admin/product/updateSpuInfo",
  // 新增 spu
  SAVESPUINFO_URL = "/admin/product/saveSpuInfo",
  // 新增 SKU
  SAVESKUINFO_URL = "/admin/product/saveSkuInfo",
  // 查看sku
  FINDBYSPUID_URL = "/admin/product/findBySpuId",
  // 删除 SPU
  DELETESPU_URL = "/admin/product/deleteSpu"
}

export const getSPUList = (page: number, limit: number, categoryId: number | string) => request.get<any,SpuListResponse>(`${API.SPULIST_URL}/${page}/${limit}?category3Id=${categoryId}`)
export const getTrademarkList = () => request.get<any, ResponseTrademark>(API.TRADEMARKLIST_URL)
export const getSpuImageList = (spuid:number) => request.get<any, ResponseSpuImage>(`${API.SPUIMAGELIST_URL}/${spuid}`)
export const getSpuSaleAttrList = (spuid: number) => request.get<any, ResponseSpuSaleAttr>(`${API.SPUSALEATTRLIST_URL}/${spuid}`)
export const getBaseSaleAttrList = () => request.get<any, ResponseBaseSaleAttrList>(API.BASESALEATTRLIST_URL)

export const addOrEditSpuInfo = (data:any) => {
  if(data.id) {
    return request.post<any,any>(API.UPDATESPUINFO_URL, data)
  } else {
    return request.post<any,any>(API.SAVESPUINFO_URL, data)
  } 
}

export const reqSaveSKUInfo = (data:any) => request.post<any, AddSkuInfo>(API.SAVESKUINFO_URL, data)

export const reqFindSku = (spuId:number|string) => request.get(`${API.FINDBYSPUID_URL}/${spuId}`)

export const reqDeleteSpu = (spuId:string | number) => request.delete(`${API.DELETESPU_URL}/${spuId}`)