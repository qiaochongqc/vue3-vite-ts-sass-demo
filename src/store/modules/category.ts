import { defineStore } from "pinia"

import { reqC1, reqC2, reqC3 } from "@/apis/product/attr"
import type { CategoryResponseData } from "@/apis/product/attr/type"
import type { CategoryState } from "./types/type"
const categoryStore = defineStore("categoryStore", {
  state: ():CategoryState => {
    return {
      c1Arr: [],
      c1Id: "",
      c2Arr: [],
      c2Id: "",
      c3Arr: [],
      c3Id: ""
    }
  },
  actions: {
    async requestCategory1() {
      let result:CategoryResponseData = await reqC1()
      if(result.code === 200) {
        this.c1Arr = result.data;
      }
    },

    async requestCategory2() {
      let result:CategoryResponseData = await reqC2(this.c1Id)
      if(result.code === 200) {
        this.c2Arr = result.data;
      }
    },

    async requestCategory3() {
      let result:CategoryResponseData = await reqC3(this.c2Id)
      if(result.code === 200) {
        this.c3Arr = result.data;
      }
    }
  }
}) 
export default categoryStore