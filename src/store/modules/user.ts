import { defineStore } from "pinia"
import { reqLogin, reqLogout, reqUserInfo } from "@/apis/user/index"
// 获取登录接口响应的数据类型
import type { loginFormData, loginResponseData, userResponseData } from "@/apis/user/type";
import { SET_TOKEN, GET_TOKEN, REMOVE_TOKEN } from "@/utils/token"

// 引入路由
import { constantRoute, asyncRoute, anyRoute } from "@/router/routes"
import router from "@/router"

import { cloneDeep } from "lodash";

const filterAsyncRouter = (asyncRoute:any, routes:any ) => {
  return asyncRoute.filter((item:any) => {
    if(routes.includes(item.name)) {
      if(item.children && item.children.length > 0) {
        item.children = filterAsyncRouter(item.children, routes)
      }
      return true;
    }
  })
}

let useUserStore = defineStore("User", {
  state: ():any => {
    return {
      // token: localStorage.getItem("token")
      token: GET_TOKEN(),
      menuRouters: constantRoute,
      username: "",
      avatar: "",
      buttons: []
    }
  },
  actions: {
    async login(formData:loginFormData) {
      let result:loginResponseData = await reqLogin(formData);
      if(result.code === 200) {
        // XXX as string 为断言，表示这里的 XXX 确定为 string 类型
        this.token = result.data as string;
        // localStorage.setItem("token", result.data.token as string)
        SET_TOKEN(result.data as string);
        return "ok"
      } else {
        return Promise.reject(new Error(result.message))
      }
    },
    async userInfo() {
      let result:userResponseData = await reqUserInfo();
      if(result.code === 200) {
        this.username = result.data.name;
        this.avatar = result.data.avatar;
        this.buttons = result.data.buttons;

        // 获取用户信息成功后，处理异步路由
        let userAsyncRoute = filterAsyncRouter(cloneDeep(asyncRoute), result.data.routes);
        this.menuRouters = [...constantRoute, ...userAsyncRoute, ...anyRoute];
        [...userAsyncRoute, ...anyRoute].forEach((route) => {
          router.addRoute(route)
        })
        // 获取注册过的所有路由
        console.log(router.getRoutes())
        return "ok"
      } else {
        return Promise.reject("获取用户信息失败！")
      }
    },
    async logout() {
      let result = await reqLogout();
      debugger
      if(result.code === 200) {
        this.username = "";
        this.avatar = "";
        this.token = "";
        REMOVE_TOKEN()
      }
    }
  }
})
export default useUserStore