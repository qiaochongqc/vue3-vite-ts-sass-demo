import request from "@/utils/request";

import type { ResponseRoleList, Role, ResponsePermissionData } from "./type"

enum API {
  // 角色列表接口
  ROLELIST_URL = "/admin/acl/role",
  // 新增角色
  ADDROLE_URL = "/admin/acl/role/save",
  // 更新角色
  UPDATEROLE_URL = "/admin/acl/role/update",
  // 根据角色获取权限
  PERMISSION_URL = "/admin/acl/permission/toAssign",
  // 给角色配置权限
  SETROLEPERMISSION_URL = "/admin/acl/permission/doAssign",
  // 删除角色
  REOMVEROLE_URL = "/admin/acl/role/remove"
}

export const reqRoleList = (page:number, limit:number, roleName:string) => request.get<any, ResponseRoleList>(`${API.ROLELIST_URL}/${page}/${limit}?roleName=${roleName}`)

export const addOrUpdateRole = (data:Role) => {
  if(data.id) {
    return request.put<any, any>(API.UPDATEROLE_URL, data)
  } else {
    return request.post<any, any>(API.ADDROLE_URL, data);
  }
}

export const reqPermission = (roleId:number) => request.get<any, ResponsePermissionData>(`${API.PERMISSION_URL}/${roleId}`)

export const reqSetRolePermission = (roleId: number, permissionIds:number[]) => request.post(`${API.SETROLEPERMISSION_URL}?roleId=${roleId}&permissionId=${permissionIds}`)

export const reqDeleteRole = (roleId:number) => request.delete<any,any>(`${API.REOMVEROLE_URL}/${roleId}`)