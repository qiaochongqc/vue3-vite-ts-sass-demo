export interface ResponseData {
  code: number,
  ok: string,
  message: string
}

export interface User {
  id?: number,
  createTime?: string,
  updateTime?: string,
  username?: string,
  password?: string,
  name?: string,
  phone?: null,
  roleName?: string
}

export type Records = User[]

export interface ResponseUserList extends ResponseData {
  data: {
    records: Records,
    total: number,
    size: number,
    current: number,
    pages: number
  }
}

export interface Role {
  id?: number,
  createTime?: string,
  updateTime?: string,
  roleName: string,
  remark: null
}

export type RoleList = Role[]

export interface RespondeUserRole extends ResponseData {
  data: {
    assignRoles: RoleList,
    allRolesList: RoleList
  }
}

export interface DoAssignParams {
  roleIdList: RoleList,
  userId: number|string
}