// 进行 axios 二次封装：使用请求与响应lan jie q
//引入 axios
import axios from "axios";
// 引入 element-plus 的提示框组件，在请求失败的时候，弹出失败信息
import { ElMessage } from "element-plus";
// 获取用户相关信息
import useUserStore from "@/store/modules/user"


// 这里通过 axios 创建的 request 实例，可以增加一些配置信息
//第一步：利用 axios 对象的create方法，创建一个axios实例（其中可配置：基础路径、超时时间）
let request = axios.create({
  // 这里的 import.meta.env.VITE_APP_BASE_API 读取到的是 .env.xxx（如.env.development） 文件中的相关配置
  baseURL: import.meta.env.VITE_APP_BASE_API, 
  // 配置超时时间
  timeout: 5000
});
// 第一步：请求拦截器
request.interceptors.request.use((config) => {
  // config 里面包含了 baseURL、method、url、headers、timeout 等
  
  // 设置请求头 token
  let userStore = useUserStore();
  if(userStore.token) {
    config.headers.token = userStore.token;
  }

  // console.log(config)
  // 这里要 return 出去，否则请求发不出去
  return config;
})
// 第二步：响应拦截器
request.interceptors.response.use((response:any) => {
  //成功的回调，作用：简化数据
  // response 中包含 请求的config（数据同第17行）、接口响应的 data、状态 status、headers等
  // console.log(response)
  // 将接口响应的数据返回
  return response.data;
}, (error) => {
  // 错误回调，处理错误信息
  // error 是接口请求失败的信息，包含 code 和 config、message、response（接口失败的信息）
  // console.log(error)
  let status = error.response.status, message = "";
  switch (status) {
    case 401: 
      message = "TOKEN过期";
      break; 
    case 403:
      message = "无权访问";
      break;
    case 404:
      message = "请求地址错误";
      break;
    case 500:
      message = "服务器出现问题";
      break;
    default:
      message = "网络出现问题";
      break;
  }
  ElMessage({
    type: "error",
    message
  })
  return Promise.reject(error);
})

export default request;