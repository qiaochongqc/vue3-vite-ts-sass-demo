import { createApp } from 'vue'
import App from '@/App.vue'
// 用来设置暗黑模式
import 'element-plus/theme-chalk/dark/css-vars.css'
import ElementPlus from "element-plus"
//@ts-ignore
import zhCn from 'element-plus/dist/locale/zh-cn.mjs'
import 'element-plus/dist/index.css'
// svg使用步骤：1:下载，2:vite.config.ts中配置，3:在入口文件导入配置项，4:使用
//导入配置项
import "virtual:svg-icons-register"
// 引入 scss
import "./style/index.scss"

//测试接口
// import axios from "axios"
// axios({
//   url: "/api/user/login",
//   method: "post",
//   data: {
//     username: 'admin',
//     password: '111111',
//   }
// })

// 这里获取 .env.development、.env.production 中的配置
// console.log(import.meta.env)

let app = createApp(App)

//注册全局组件
//方法1: 如果要注册很多全局组件，这里就需要引入、注册多次，比较麻烦
// import SvgIcon from '@/components/SvgIcon/index.vue'
// app.component("SvgIcon",SvgIcon)

//方法2:components 下新增 index.ts，在里面引入组件，遍历注册组件
//引入自定义插件对象，注册整个项目全局组件
import gloablComponent from "@/components"
// console.log(gloablComponent)
//安装自定义插件（gloablComponent是带有 install 方法的一个对象，安装gloablComponent以后，初始化时，会执行install方法）
app.use(gloablComponent)

app.use(ElementPlus, {
  locale: zhCn
})


import { isHasButton } from "./directive/has"
isHasButton(app);

// 引入路由
import router from "./router"
//引入并注册状态管理工具 pinia
import { createPinia } from "pinia";
const pinia = createPinia()
app.use(pinia)


//注册路由
app.use(router)

// 引入路由全局守卫
import "@/router/permission"

app.mount('#app')

