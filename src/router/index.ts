// 引入创建路由实例的方法，和创建 hash(createWebHashHistory) / history(createWebHistory) 模式的方法
import { createRouter, createWebHashHistory } from "vue-router"

import { constantRoute } from "./routes.ts"
let router = createRouter({
  // 配置路由模式：hash
  history: createWebHashHistory(),
  // 配置路由
  routes: constantRoute,
  //路由切换时，页面滚动到顶部左侧
  scrollBehavior() {
    return {
      top: 0,
      left: 0
    }
  }
});
export default router;