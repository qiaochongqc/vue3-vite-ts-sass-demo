// 统一管理项目用户相关的接口
import request from "@/utils/request";
// import 语法会将整个模块文件加载进去
// import type 只会从模块中获取类型信息
import type { loginFormData, loginResponseData, userResponseData } from "./type";


// enum API {
//   LOGIN_URL = "/user/login",
//   USERINFO_URL = "/user/info"
// }

// export const reqLogin = (data:loginForm) => request.post<any,loginResponseData>(API.LOGIN_URL, data)
// export const reqUserInfo = () => request.get<any,userResponseData>(API.USERINFO_URL)

enum API {
  LOGIN_URL = "/admin/acl/index/login",
  LOGOUT_URL = "/admin/acl/index/logout",
  USERINFO_URL = "/admin/acl/index/info",
}

export const reqLogin = (data:loginFormData) => request.post<loginResponseData>(API.LOGIN_URL, data)
export const reqLogout = () => request.post<any>(API.LOGOUT_URL)
export const reqUserInfo = () => request.get<userResponseData>(API.USERINFO_URL)